# Sotheby's Cloud Formation Templates

## Overview

Template | Suggested Stack Name | Description
--- | --- | ---
[sothebys-ecs-cluster.cf.yaml](sothebys-ecs-cluster.cf.yaml) | `stb-ecs-{ENV}` | ECS clusters with all infrastructural components.
[sothebys-jenkins-server.cf.yaml](sothebys-jenkins-server.cf.yaml) | `stb-jenkins` | Jenkins server.
[stb-images.cf.yaml](stb-images.cf.yaml) | `stb-stk-{ENV}-images` | Sotheby's global images bucket.
